﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CompteEstBon
{
    public partial class Form3 : Form
    {
        private System.Windows.Forms.Button[] txtPlq;

        public Form3()
        {
            InitializeComponent();
            CreerPlaquettes();
            AffichePlaquettes(6);
            InitialiserPlaquettes();
            GenerateRandomNumber();
        }

        // Les 10 plaquettes sont créées une fois pour toute
        void CreerPlaquettes()
        {
            txtPlq = new Button[10];
            for (int i = 0; i < 10; i++)
            {
                this.txtPlq[i] = new Button();
                // this.txtPlq1.Location = new System.Drawing.Point(3, 3);
                this.txtPlq[i].Name = "txtPlq" + i;
                this.txtPlq[i].Size = new System.Drawing.Size(48, 20);
                this.txtPlq[i].TabIndex = i;
            }
        }
        /// <summary>
        /// Insertion dynamique de TextBox dans le container PanelPlaquettes en fonction du nombre de plaques choisies
        /// </summary>
        /// <param name="nb">nbre de plaquettes</param>
        private void AffichePlaquettes(int nb)
        {   // SuspendLayout et PerformLayout permettent de controler la logique de presentation
            // On applique la logique qu'après avoir inseré tous les controles dans le container
            this.PanelPlaquettes.SuspendLayout();
            this.PanelPlaquettes.Controls.Clear();  // Enleve toutes les anciennes plaquettes du container 
            for (int i = 0; i < nb; i++)
                this.PanelPlaquettes.Controls.Add(txtPlq[i]);

            this.PanelPlaquettes.ResumeLayout(false);
            this.PanelPlaquettes.PerformLayout();
        }

        private void InitialiserPlaquettes()
        {
            int[] tab = new int[14];
            int i = 0;
            int j = 0;
            Random random = new Random();

            for (i=0; i<tab.Length; i++)
            {
                tab[i] = 2;
            }

            for (j=0; j < 6; j++)
            {  
                int rand = random.Next(0, 14);
                while (tab[rand] == 0)
                {
                    rand = random.Next(0, 14);
                }
            
                switch (rand)
                {
                    case 10:
                        this.txtPlq[j].Text = 25.ToString();
                        tab[rand]--;
                        break;
                    case 11:
                        this.txtPlq[j].Text = 50.ToString();
                        tab[rand]--;
                        break;
                    case 12:
                        this.txtPlq[j].Text = 75.ToString();
                        tab[rand]--;
                        break;
                    case 13:
                        this.txtPlq[j].Text = 100.ToString();
                        tab[rand]--;
                        break;
                    default:
                        this.txtPlq[j].Text = (rand+1).ToString();
                        tab[rand]--;
                        break;
                }
            }
        }

        private void GenerateRandomNumber()
        {
            Random random = new Random();
            txtRes.Text = random.Next(100, 999).ToString();
        }

        private void BtnAuRevoir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
