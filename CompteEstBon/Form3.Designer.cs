﻿namespace CompteEstBon
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnAuRevoir = new System.Windows.Forms.Button();
            this.PanelPlaquettes = new System.Windows.Forms.FlowLayoutPanel();
            this.txtRes = new System.Windows.Forms.Label();
            this.numberToFind = new System.Windows.Forms.Label();
            this.btnPlus = new System.Windows.Forms.Button();
            this.BtnMoins = new System.Windows.Forms.Button();
            this.BtnFois = new System.Windows.Forms.Button();
            this.BtnDiviser = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BtnAuRevoir
            // 
            this.BtnAuRevoir.Location = new System.Drawing.Point(202, 336);
            this.BtnAuRevoir.Name = "BtnAuRevoir";
            this.BtnAuRevoir.Size = new System.Drawing.Size(75, 23);
            this.BtnAuRevoir.TabIndex = 0;
            this.BtnAuRevoir.Text = "Au Revoir !";
            this.BtnAuRevoir.UseVisualStyleBackColor = true;
            this.BtnAuRevoir.Click += new System.EventHandler(this.BtnAuRevoir_Click);
            // 
            // PanelPlaquettes
            // 
            this.PanelPlaquettes.Location = new System.Drawing.Point(73, 91);
            this.PanelPlaquettes.Name = "PanelPlaquettes";
            this.PanelPlaquettes.Size = new System.Drawing.Size(368, 44);
            this.PanelPlaquettes.TabIndex = 1;
            // 
            // txtRes
            // 
            this.txtRes.AutoSize = true;
            this.txtRes.Location = new System.Drawing.Point(56, 235);
            this.txtRes.Name = "txtRes";
            this.txtRes.Size = new System.Drawing.Size(37, 13);
            this.txtRes.TabIndex = 3;
            this.txtRes.Text = "txtRes";
            // 
            // numberToFind
            // 
            this.numberToFind.AutoSize = true;
            this.numberToFind.Location = new System.Drawing.Point(56, 213);
            this.numberToFind.Name = "numberToFind";
            this.numberToFind.Size = new System.Drawing.Size(98, 13);
            this.numberToFind.TabIndex = 4;
            this.numberToFind.Text = "Nombre à trouver : ";
            // 
            // btnPlus
            // 
            this.btnPlus.Location = new System.Drawing.Point(82, 141);
            this.btnPlus.Name = "btnPlus";
            this.btnPlus.Size = new System.Drawing.Size(35, 34);
            this.btnPlus.TabIndex = 5;
            this.btnPlus.Text = "+";
            this.btnPlus.UseVisualStyleBackColor = true;
            // 
            // BtnMoins
            // 
            this.BtnMoins.Location = new System.Drawing.Point(168, 141);
            this.BtnMoins.Name = "BtnMoins";
            this.BtnMoins.Size = new System.Drawing.Size(35, 34);
            this.BtnMoins.TabIndex = 6;
            this.BtnMoins.Text = "-";
            this.BtnMoins.UseVisualStyleBackColor = true;
            // 
            // BtnFois
            // 
            this.BtnFois.Location = new System.Drawing.Point(252, 141);
            this.BtnFois.Name = "BtnFois";
            this.BtnFois.Size = new System.Drawing.Size(35, 34);
            this.BtnFois.TabIndex = 7;
            this.BtnFois.Text = "x";
            this.BtnFois.UseVisualStyleBackColor = true;
            // 
            // BtnDiviser
            // 
            this.BtnDiviser.Location = new System.Drawing.Point(337, 141);
            this.BtnDiviser.Name = "BtnDiviser";
            this.BtnDiviser.Size = new System.Drawing.Size(35, 34);
            this.BtnDiviser.TabIndex = 8;
            this.BtnDiviser.Text = "/";
            this.BtnDiviser.UseVisualStyleBackColor = true;
            // 
            // Form3
            // 
            this.ClientSize = new System.Drawing.Size(463, 406);
            this.Controls.Add(this.BtnDiviser);
            this.Controls.Add(this.BtnFois);
            this.Controls.Add(this.BtnMoins);
            this.Controls.Add(this.btnPlus);
            this.Controls.Add(this.numberToFind);
            this.Controls.Add(this.txtRes);
            this.Controls.Add(this.PanelPlaquettes);
            this.Controls.Add(this.BtnAuRevoir);
            this.Name = "Form3";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button BtnAuRevoir;
        private System.Windows.Forms.FlowLayoutPanel PanelPlaquettes;
        private System.Windows.Forms.Label txtRes;
        private System.Windows.Forms.Label numberToFind;
        private System.Windows.Forms.Button btnPlus;
        private System.Windows.Forms.Button BtnMoins;
        private System.Windows.Forms.Button BtnFois;
        private System.Windows.Forms.Button BtnDiviser;
    }
}