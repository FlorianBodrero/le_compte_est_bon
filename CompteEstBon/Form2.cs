﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CompteEstBon
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.btnSolo = new System.Windows.Forms.Button();
            this.btnIA = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnSolo
            // 
            this.btnSolo.Location = new System.Drawing.Point(88, 157);
            this.btnSolo.Name = "btnSolo";
            this.btnSolo.Size = new System.Drawing.Size(91, 63);
            this.btnSolo.TabIndex = 0;
            this.btnSolo.Text = "Jouer Seul";
            this.btnSolo.UseVisualStyleBackColor = true;
            this.btnSolo.Click += new System.EventHandler(this.btnSolo_Click);
            // 
            // btnIA
            // 
            this.btnIA.Location = new System.Drawing.Point(279, 157);
            this.btnIA.Name = "btnIA";
            this.btnIA.Size = new System.Drawing.Size(96, 63);
            this.btnIA.TabIndex = 1;
            this.btnIA.Text = "L\'IA joue";
            this.btnIA.UseVisualStyleBackColor = true;
            this.btnIA.Click += new System.EventHandler(this.btnIA_Click);
            // 
            // Form2
            // 
            this.ClientSize = new System.Drawing.Size(477, 358);
            this.Controls.Add(this.btnIA);
            this.Controls.Add(this.btnSolo);
            this.Name = "Form2";
            this.ResumeLayout(false);

        }

        private void btnSolo_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form3 nav = new Form3();
            nav.Closed += (s, args) => this.Close();
            nav.Show();
        }

        private void btnIA_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 nav = new Form1();
            nav.Closed += (s, args) => this.Close();
            nav.Show();
        }
    }
}
